import { shallowMount } from '@vue/test-utils';
import Error from '@/components/Error.vue';

describe('Error.vue', () => {
  it('default text', () => {
    const wrapper = shallowMount(Error);
    expect(wrapper.text()).toMatch('No data');
  });
  it('custom message', () => {
    const text = 'Custom Message props';
    const wrapper = shallowMount(Error, {
      propsData: { text },
    });
    expect(wrapper.text()).toMatch(text);
  });
  it('dark style', () => {
    const dark = true;
    const wrapper = shallowMount(Error, {
      propsData: { dark },
    });
    const darkClass = wrapper.find('.dark');
    expect(darkClass.exists()).toBe(true);
  });
});

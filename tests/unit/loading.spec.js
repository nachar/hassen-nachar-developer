import { shallowMount } from '@vue/test-utils';
import Loading from '@/components/Loading.vue';

describe('Loading.vue', () => {
  it('default style', () => {
    const wrapper = shallowMount(Loading);
    const div = wrapper.find('.loading-default');
    expect(div.exists()).toBe(true);
  });
  it('dark style', () => {
    const dark = true;
    const wrapper = shallowMount(Loading, {
      propsData: { dark },
    });
    const darkClass = wrapper.find('.dark');
    expect(darkClass.exists()).toBe(true);
  });
});

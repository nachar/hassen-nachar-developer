module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/scss/custom.scss";',
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
};

import Vue from 'vue';
import vueSmoothScroll from 'vue2-smooth-scroll';
import App from './App.vue';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import i18n from './i18n';

Vue.use(vueSmoothScroll);

Vue.config.productionTip = false;

new Vue({
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');

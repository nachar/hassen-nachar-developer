import Vue from 'vue';
import Vuex from 'vuex';
import jobs from './modules/jobs';
import projects from './modules/projects';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    jobs,
    projects,
  },
});

/* eslint-disable no-shadow */
import $api from '@/api';

const state = {
  jobs: undefined,
};

// getters
const getters = {
  jobs: (state) => state.jobs,
};

// actions
const actions = {
  GET_JOBS(context) {
    return new Promise((resolve, reject) => {
      $api.get('jobs?filter[order]=id%20DESC')
        .then((jobs) => {
          context.commit('SET_JOBS', jobs.data);
          resolve(jobs.data);
        }, (err) => {
          reject(err);
        });
    });
  },
};

// mutations
const mutations = {
  SET_JOBS(state, jobs) {
    state.jobs = jobs;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

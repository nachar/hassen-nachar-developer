/* eslint-disable no-shadow */
import $api from '@/api';

const state = {
  projects: undefined,
};

// getters
const getters = {
  projects: (state) => state.projects,
};

// actions
const actions = {
  GET_PROJECTS(context) {
    return new Promise((resolve, reject) => {
      $api.get('projects?filter[order]=id%20DESC')
        .then((projects) => {
          context.commit('SET_PROJECTS', projects.data);
          resolve(projects.data);
        }, (err) => {
          reject(err);
        });
    });
  },
};

// mutations
const mutations = {
  SET_PROJECTS(state, projects) {
    state.projects = projects;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
